""" 
Simple Sorting Algorithms Class
"""

import time
import random 

class SortingAlgorithms:

    def __init__(self):
        pass
        

    def __str__(self):
        return """ Sorting Algorithms Implementations """


    def bubble_sort(self, random_list):
        """ Implementing Bubble Sort Algorithm """
        print(f"Sorting: {random_list}")
        switch = True
        while switch:
            switch = False
            for i in range(len(random_list)-1):
                if random_list[i] > random_list[i+1]:
                    random_list[i], random_list[i+1] = random_list[i+1], random_list[i]
                    switch = True
                
        return random_list
       

    def selection_sort(self, random_list):
        """ Implementing Selection Sort """
        print(f"Sorting: {random_list}")
        for index in range(len(random_list)):
            for comp_index in range(index, len(random_list)):
                if random_list[index] > random_list[comp_index]:
                    random_list[index], random_list[comp_index] = random_list[comp_index], random_list[index]

        return random_list


    def insertion_sort(self, random_list):
        """ Implementing Insertion Sort """
        print(f"Sorting: {random_list}")
        for i in range(1, len(random_list)):
            curr_key = i
            
            while curr_key > 0 and random_list[curr_key] <= random_list[curr_key-1]:
                random_list[curr_key], random_list[curr_key-1] = random_list[curr_key-1], random_list[curr_key]
                curr_key -= 1
            
        return random_list
                    

    def quick_sort(self, random_list):
        """ Implementing Quick Sort """
        if len(random_list) < 2:
            return random_list
        else:
            pivot = random_list[-1]
            low, equal, high = [], [], []
            for num in random_list:
                if num < pivot:
                    low.append(num)
                elif num == pivot:
                    equal.append(num)
                else:
                    high.append(num)

        return self.quick_sort(low) + equal + self.quick_sort(high)
                
