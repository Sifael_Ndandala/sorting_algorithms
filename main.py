
""" 
Main Script: 

1. Generates a random list of ints based on user input or custom list.
2. Users the sorting algorithms class to sort the data.
"""


import random
from algo_analyzer import SortingAlgorithms



def GenerateList(size, max_value):

    if size > 10000:
        print("The list is too large for these algorithms. Perhaps use the python native algorithm")
        return None
    else:    
        random_list = []
        for _ in range(max_value+1):
            random_list.append(random.randint(0, max_value))

        return random_list



if __name__ == "__main__":
    print(f"Sorting Algorithms")


    list_size = int(input("What is the size of the list: "))
    max_value = int(input("What is the max value of the list: ")) 

    random_list = GenerateList(list_size, max_value)

    sorting_object = SortingAlgorithms()
    sorted_list = sorting_object.bubble_sort(random_list.copy())
    print(f"Sorted List: {sorted_list}")